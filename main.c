	/** @example examples/ble_peripheral/ble_app_buttonless_dfu
	*
	* @brief Secure DFU Buttonless Service Application main file.
	*
	* This file contains the source code for a sample application using the proprietary
	* Secure DFU Buttonless Service. This is a template application that can be modified
	* to your needs. To extend the functionality of this application, please find
	* locations where the comment "// YOUR_JOB:" is present and read the comments.
	*/
	/*
	*   Version 1.0:  	(DRAFT)  contains main functionnality but custo charact no implemented
	*   Version 1.1:  	(STABLE) first version 
	*   Version 1.2:  	(STABLE) adding battery level characteristic + batt voltage sampling
	*   Version 1.3:  	(STABLE) adding BLE code to tweak config  parameters (like accelero threshold ...)
	*   Version 1.4:  	(STABLE) put battery level in UART param instead of BLE service	
	*   Version 1.5:  	(STABLE) transform to ASCCI what is sent back over BLE to Victorise App (via code '8')	
	*   Version 1.6:  	(STABLE) improve output Rx packet
	*   Version 1.7:  	(STABLE) set no latch on interrupt INT1 
	*   Version 1.8:  	(STABLE) change RGB LED from "program mode" to "direct mode"
	*   Version 1.9:  	(STABLE) change color = '1' for red, '2' green and '3' blue 
	*   Version 1.10: 	(STABLE) add colors 'yellow' 'orange'... + force go to sleep when sending '0' in BLE packet
	*   Version 1.11: 	(STABLE) for crash detection: move all existing functionalities (sleep and wakeup) to INT1 pin only (instead of INT1 and INT2) and INT2 is used for crash detection
	*   Version 1.12: 	(STABLE) drop red led(bug in pcb) / manage charging PIN / optimize voltage sampling
	*   Version 1.13: 	(STABLE) implementation of data saving in the Flash memory to keep customized value even after power off
	*   Version 1.20: 	(STANDBY) when charging, change voltage range and colors upon battery voltage
	*   Version 1.21SLEEP: 	(TEST ONLY) going to sleep immediately + APP_ADV_DURATION set to 0.5 seconds
	*   Version 1.22: 	(TEMP) wait 30 sec before waking up + APP_ADV_DURATION set to 0.5 seconds 
	*   Version 1.23: 	(TEMP) wait 30 sec before waking up + APP_ADV_DURATION set to 0.5 seconds + wake up only if Vdd >= 2.9V	+ shipping mode
	*   Version 1.24: 	(TEMP) when LED is lighted up permanently, shut down sleep mode	
	*   Version 1.25: 	(TEMP) new FW for HW version X1	
	*/
#define FW_VERSION_MAJOR 1  
#define FW_VERSION_MINOR 24  //version can be checked over BLE (see code 80R00)
/*
	Victorise customization:

- sdk_config.h : NRF_SDH_BLE_VS_UUID_COUNT must reflect number of UUIDS 
- sdk_config.h : for TWI, in sdk_config.h, TWI_DEFAULT_CONFIG_CLR_BUS_INIT = 0
- sdk_config.h : for TWI, TWI0_USE_EASY_DMA = 0

- PREPROCESSOR:  as the PIN used for I2C are the default NFCT pins of nRF52832 then the preprocessor
  must contain CONFIG_NFCT_PINS_AS_GPIOS
*/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nrf_dfu_ble_svci_bond_sharing.h"
#include "nrf_svci_async_function.h"
#include "nrf_svci_async_handler.h"

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "bsp_btn_ble.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_state.h"
#include "ble_dfu.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "fds.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_clock.h"
#include "nrf_power.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_bootloader_info.h"
#include "ble_nus.h"   //+VICTORISE
#include "ble_bas.h" //+VICTORISE
#include "app_uart.h"  //+VICTORISE
#include "nrf_drv_twi.h" //+VICTORISE
#include "nrf_delay.h" //+VICTORISE
#include "ctype.h" //+VICTORISE
#include "nrf_drv_gpiote.h"  //+VICTORISE accelero 
#include "nrf_drv_saadc.h" //+VICTORISE sample adc voltage batt
#include "nrf_fstorage.h"  //for storing data in Flash rom
#include "nrf_fstorage_sd.h"  //for storing data in Flash rom

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#define DEVICE_NAME                     "EYECO-01"                      						/**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "Nordic"                                    /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                300                                         /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
//#define APP_ADV_DURATION                6000                                       /**< The advertising duration (180 seconds) in units of 10 milliseconds. */
#define APP_ADV_DURATION              18000                                       /**50 > 0.5 secs */

#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)            /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)            /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                           /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                                   /**< BLE NUS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */
BLE_BAS_DEF(m_bas);                                                 								/**< Structure used to identify the battery service. */

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                            /**< Handle of the current connection. */
static void advertising_start();                                    /**< Forward declaration of advertising start function */

// YOUR_JOB: Use UUIDs for service(s) used in your application.
static ble_uuid_t m_adv_uuids[] = {
			//{BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE},
			{BLE_UUID_NUS_SERVICE, BLE_UUID_TYPE_BLE}
			//{BLE_UUID_BATTERY_SERVICE, BLE_UUID_TYPE_BLE}   //not necessary, if present then the  device name is truncated
};
//static ble_uuid_t m_adv_uuids[] = { {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}};

//begin victorise
static uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
//end victorise

/****** BEGIN VICTORISE ***********************/

#define FIXED_TIMEOUT_VALUE 50 // ==> don't change it! ... 50 milliseconds timer timeout value (minimum step) used for all the timers there
#define FIXED_TIMEOUT_VALUE_1S 1000 //==> don't change it! ... it's 1000ms for the wakeup mode of the device

//-- BEGIN RGB DRIVER PART -------------------------/
#define RGB_DRIV_I2C_ADDR     0x32
#define DEMO_MODE_DURATION_MIN 5    //in minutes ==> value can be changed
#define G_DEMO_MODE_TOTAL_TIME_COUNTER_VAL (DEMO_MODE_DURATION_MIN*60*(1000/FIXED_TIMEOUT_VALUE))

#define DEMO_MODE_BLINK_DURATION_SEC 2.5  //in seconds  ==> value can be changed
#define G_DEMO_MODE_BLINK_DURATION_COUNTER_VAL (DEMO_MODE_BLINK_DURATION_SEC*(1000/FIXED_TIMEOUT_VALUE))

#define I2C_DELAY     10  // mandatory delay in ms for this interface I2C else the target blocks .... CANNOT be set lower than 10ms

APP_TIMER_DEF(wakeup_mode_timer);   //Macro to define the main timer for device active mode 
APP_TIMER_DEF(rgb_display_timer);   //Macro to define the timer for rgb led displaying
APP_TIMER_DEF(rgb_display_demo_mode_timer);   //Macro to define the timer for rgb led displaying in demo mode


//--- END RGB DRIVER PART -------------------------/

//-- BEGIN ACCELERO PART -------------------------/
#define ACCEL_I2C_ADDR      0x18

//in seconds, time to remain active after last interrupt "active" has been sent by accelero
// SLEEP_MODE_TIMEOUT_COUNTER is uint16_t (unsigned char (16-bit)) so 65535 max //
////#define SLEEP_MODE_TIMEOUT_COUNTER_VALUE  7200  // duration in seconds the device stays in wakeup mode 
#define SLEEP_MODE_TIMEOUT_COUNTER_VALUE  7200  // duration in seconds the device stays in wakeup mode 

#define 	WHO_AM_I	0x0F
#define 	CTRL_REG1	0x20
#define 	CTRL_REG2	0x21
#define 	CTRL_REG3	0x22
#define 	CTRL_REG4	0x23
#define 	CTRL_REG5	0x24
#define 	CTRL_REG6	0x25
#define 	INT1_CFG	0x30
#define 	INT1_SRC	0x31
#define 	INT1_THS	0x32
#define 	INT1_DURATION	0x33
#define 	INT2_CFG	0x34
#define 	INT2_SRC	0x35
#define 	INT2_THS	0x36
#define 	INT2_DURATION	0x37

static uint8_t G_ACCELERO_THRESHOLD_INT1=0x03;   //threshold for INT1 (wake up), prefered value is 0x0C @FS=2g
static uint8_t G_ACCELERO_THRESHOLD_INT2=0x45;   //threshold for INT2 (crash test) 

//-- END ACCELERO PART -------------------------/

//-- BEGIN BATTERY VOLTAGE SAMPLING ----------------/
#define SAMPLES_IN_BUFFER 1
static bool G_FLAG_BATTERY_OK = false; 
static bool G_FLAG_INIT_DONE = false; 
//-- END BATTERY VOLTAGE SAMPLING ------------------/

//-- BEGIN FLASH MEMORY STORAGE ----------/
// the area must be inside and at the end of the FW ROM area (see tab "Target" in Keil)
#define FSTORAGE_START_ADDR	0x38000  
#define FSTORAGE_END_ADDR		0x40000

typedef struct
{
		char error_id;     // unique id of the error
		uint8_t err_code;	 // error generated
    uint8_t output_data;  // data returned in case of 'Read'
} flash_storage_t;

//-- END FLASH MEMORY STORAGE ------------/

static uint32_t G_DEMO_MODE_TOTAL_TIME_COUNTER=0;
static uint32_t G_NORMAL_MODE_TOTAL_TIME_COUNTER=0;
static uint16_t G_NORMAL_MODE_LIGHT_TIME_COUNTER=0;
static uint16_t G_NORMAL_MODE_LIGHT_TIME_COUNTER_VAL=0;
static bool 		G_BLINK_MODE;
static bool 		G_TURNED_OFF_MODE;
static bool 		G_LIGHT_LED_ON=false;
static bool     G_START_WAKEUP_MODE_TIMER=true;
static bool 		G_FLAG_SHUT_DOWN_SLEEP_MODE=false;
static uint16_t G_DEMO_MODE_BLINK_COUNTER=0;
static uint8_t 	G_COLOR=1;
static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];

static bool volatile m_fds_initialized;  /* Flag to check fds initialization. */

//This global variable allows for "live viewing of the battery voltage from the debugger.
float batteryVoltage = -1;

void timer_timeout_handler_wakeup_mode(void * p_context);  // timer for victorise wakeup mode
void timer_timeout_handler_rgb_display(void * p_context);  // timer for victorise rgb led displaying
void timer_timeout_handler_rgb_display_demo_mode(void * p_context);  // timer for victorise rgb led displaying
void prepare_sleep_mode (void);
void RGB_driver_display_mode(uint8_t COLOR);
void RGB_driver_demo_mode(void);
void send_I2C_packet(uint8_t *reg);
void turn_off_RGB_color(void);
void enable_RGB_color(void);
void sleep_mode_enter(void);
void manage_uart_message(char * message,uint16_t message_length);
void manage_rgb_current(uint8_t current);
void init_all_services(void);
void saadc_init(void);

static uint16_t sleep_mode_timeout_counter=0; //variable for time to remain active before going Sleep mode, uint16_t unsigned short (16-bit) 
static uint16_t timeout_counter_10sec=0; // a counter every 10 seconds
static uint16_t previous_sleep_mode_timeout_counter=0; //used when to light up the rgb when charging

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

static void manage_device_config(char *, uint16_t );
static uint8_t convert_to_hex(char *, char *);
static uint8_t convert_to_ascii(uint8_t *);

static flash_storage_t save_data_in_flash(char, uint8_t, uint32_t );
static uint32_t nrf5_flash_end_addr_get(void);
static void wait_for_flash_ready(nrf_fstorage_t const *);

/* TWI instance. */
#define TWI_INSTANCE_ID     0
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/****** END VICTORISE ***********************/

/**@brief Handler for shutdown preparation.
 *
 * @details During shutdown procedures, this function will be called at a 1 second interval
 *          untill the function returns true. When the function returns true, it means that the
 *          app is ready to reset to DFU mode.
 *
 * @param[in]   event   Power manager event.
 *
 * @retval  True if shutdown is allowed by this power manager handler, otherwise false.
 */
static bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_DFU:
            NRF_LOG_INFO("Power management wants to reset to DFU mode.");
            // YOUR_JOB: Get ready to reset into DFU mode
            //
            // If you aren't finished with any ongoing tasks, return "false" to
            // signal to the system that reset is impossible at this stage.
            //
            // Here is an example using a variable to delay resetting the device.
            //
            // if (!m_ready_for_reset)
            // {
            //      return false;
            // }
            // else
            //{
            //
            //    // Device ready to enter
            //    uint32_t err_code;
            //    err_code = sd_softdevice_disable();
            //    APP_ERROR_CHECK(err_code);
            //    err_code = app_timer_stop_all();
            //    APP_ERROR_CHECK(err_code);
            //}
            break;

        default:
            // YOUR_JOB: Implement any of the other events available from the power management module:
            //      -NRF_PWR_MGMT_EVT_PREPARE_SYSOFF
            //      -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
            //      -NRF_PWR_MGMT_EVT_PREPARE_RESET
            return true;
    }

    NRF_LOG_INFO("Power management allowed to reset to DFU mode.");
    return true;
}

//lint -esym(528, m_app_shutdown_handler)
/**@brief Register application shutdown handler with priority 0.
 */
NRF_PWR_MGMT_HANDLER_REGISTER(app_shutdown_handler, 0);


static void buttonless_dfu_sdh_state_observer(nrf_sdh_state_evt_t state, void * p_context)
{
    if (state == NRF_SDH_EVT_STATE_DISABLED)
    {
        // Softdevice was disabled before going into reset. Inform bootloader to skip CRC on next boot.
        nrf_power_gpregret2_set(BOOTLOADER_DFU_SKIP_CRC);

        //Go to system off.
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
    }
}

/* nrf_sdh state observer. */
NRF_SDH_STATE_OBSERVER(m_buttonless_dfu_state_obs, 0) =
{
    .handler = buttonless_dfu_sdh_state_observer,
};

// YOUR_JOB: Update this code if you want to do anything given a DFU event (optional).
/**@brief Function for handling dfu events from the Buttonless Secure DFU service
 *
 * @param[in]   event   Event from the Buttonless Secure DFU service.
 */
static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
            NRF_LOG_INFO("Device is preparing to enter bootloader mode.");
            // YOUR_JOB: Disconnect all bonded devices that currently are connected.
            //           This is required to receive a service changed indication
            //           on bootup after a successful (or aborted) Device Firmware Update.
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}

/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_evt_t * p_evt)
//static void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length)
{

    if (p_evt->type == BLE_NUS_EVT_RX_DATA)
    {

				/*************** BEGIN VICTORISE **********************/
				char message[9];              
				uint16_t message_length = p_evt->params.rx_data.length;  //it's a pointer!
		
				for (uint32_t i = 0; i < p_evt->params.rx_data.length; i++) message[i] = p_evt->params.rx_data.p_data[i];				
				
				manage_uart_message(message,message_length);
			/*************** END VICTORISE **********************/
    }
}

static void manage_uart_message(char * message,uint16_t message_length)
{
	
		nrf_gpio_pin_write(LED_EN,1); //enable driver
		nrf_delay_ms(1); //1 ms delay 
	
		//enable or re-enable the chip else Colors don't show
		enable_RGB_color();

		switch (message[0])
		{
		case '0': //RESET the board
				//set_enter_bootloader(true);
				//sd_nvic_SystemReset();
				//nrf_bootloader_app_start();
				prepare_sleep_mode();
				sleep_mode_enter();
				break;
		
		case '2': // a quick message to check that RGB works
				
				message[1] = 1; //red
				message[2] = '4'; // 500ms
				message[3] = message[4] = message[6] = message[7] = message[8] = '0';
				message[5] = '5'; //5 seconds	
				//no break here because it goes to '1'
		
		case '1': //Normal mode
			
				uint16_t PERIOD_CODE=0;
				uint32_t DURATION=0;
		
				// 31 31 33 30 30 35 30 30 30 // red; 200ms; 5 seconds
				// 31 33 33 30 30 30 30 30 30 // blue; 200ms; infinite
				//char message[9] = "123009000"; //green;200ms;for 9 seconds
				//char message[9] = "133010000"; //blue;200ms;for 10 seconds	

				turn_off_RGB_color();	//shut down all the colors before doing anything
		
				G_COLOR = (message[1] - '0'); //the color param
				if (G_COLOR >= 0 && G_COLOR <=9 ) { } //ok expected value
				else 	G_COLOR = 1; //red by default
				
				switch (message[2])
				{
					case '0': PERIOD_CODE=0;break; // ==> no blinking, just turned on
					case '1': PERIOD_CODE=100;break;  // ==> 100ms (because 50ms is too short for RGB driver to reply correctly)
					case '2':	PERIOD_CODE=100;break;  // ==> 100ms
					case '3': PERIOD_CODE=200;break;  // ==> 200ms
					case '4': PERIOD_CODE=500;break;  // ==> 500ms
					case '5': PERIOD_CODE=1000;break;  // ==> 1000ms
					default:  PERIOD_CODE=500;break;  // ==> 500ms
				}

				if ( isdigit(message[3]) && isdigit(message[4]) && isdigit(message[5])&& isdigit(message[6]) && isdigit(message[7]) && isdigit(message[8]) ) 
				{
						
					DURATION  =  (message[3] - '0') * 100000
										 + (message[4] - '0') * 10000
										 + (message[5] - '0') * 1000
										 + (message[6] - '0') * 100
										 + (message[7] - '0') * 10
										 + (message[8] - '0') * 1;
				}	

				RGB_driver_display_mode(G_COLOR);

				if 	(DURATION !=0)
				{			
				
						G_NORMAL_MODE_TOTAL_TIME_COUNTER = DURATION / FIXED_TIMEOUT_VALUE; //total time to blink
						G_NORMAL_MODE_LIGHT_TIME_COUNTER = G_NORMAL_MODE_LIGHT_TIME_COUNTER_VAL = PERIOD_CODE / FIXED_TIMEOUT_VALUE;  //time in ms when the Light is ON (followed by same time being Off) 
						
						G_LIGHT_LED_ON=true;
						
						app_timer_start(rgb_display_timer,APP_TIMER_TICKS(FIXED_TIMEOUT_VALUE), NULL);  //timer_timeout_handler_rgb_display
				} 
				else {G_FLAG_SHUT_DOWN_SLEEP_MODE=true;}
				break;		
		
		case '7': //shipping mode -> disable the accelerometer  and go to sleep mode				
				
				prepare_sleep_mode();
				sleep_mode_enter();
				nrf_drv_gpiote_in_event_disable(ACC_INT1);	
				nrf_drv_gpiote_in_event_disable(ACC_INT2);	
				break;		

		case '8': //Configuration mode is to set and get some parameters
			
				manage_device_config(message,message_length);
				break;
		
		case '9': //DEMO mode: the LED RED then GREEN then BLUE are displayed in sequence

//						// re-enabling the chip seems mandatory else some Color does not show
//						reg[0] = 0x00;
//						reg[1] = 0x40;
//						err_code = nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
//						nrf_delay_ms(I2C_DELAY);	
//						while (m_xfer_done == false);

				G_COLOR=1; //we begin by RED color

				G_DEMO_MODE_TOTAL_TIME_COUNTER = G_DEMO_MODE_TOTAL_TIME_COUNTER_VAL;	//total time the demo mode runs (e.g. 5 minutes)
		
				RGB_driver_demo_mode();

				app_timer_start(rgb_display_demo_mode_timer,APP_TIMER_TICKS(FIXED_TIMEOUT_VALUE), NULL);	//timer_timeout_handler_rgb_display_demo_mode		
	
				break;
	}

}

/**@brief Function for timer handler wakeup mode
 */
void timer_timeout_handler_wakeup_mode(void * p_context)
{
      
	sleep_mode_timeout_counter--;
	
	timeout_counter_10sec++;
	
  if ((sleep_mode_timeout_counter<=0) && (!G_FLAG_SHUT_DOWN_SLEEP_MODE))
	{

	  prepare_sleep_mode();
		sd_power_system_off(); //go in Low power mode
		//nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
	}		

	if (!G_FLAG_INIT_DONE)
	{	
		
//		if 	(	timeout_counter_10sec >= 10)   //10 seconds  delay
//		{			
//			timeout_counter_10sec = 0;
//			
//			nrf_gpio_cfg_output(BATT_SAM_EN);	 //+VICTORISE - PIN Enable for measuring battery level  
//			saadc_init(); //+VICTORISE - for batt voltage sampling	
//			//nrf_gpio_pin_write(BATT_SAM_EN,1); 			
//			nrf_delay_ms(200); //wait a delay before sampling			
//			nrf_drv_saadc_sample();		
//		}	
		
		G_FLAG_BATTERY_OK = true;
		
		if (G_FLAG_BATTERY_OK)
		{	
			//init_all_services();
			G_FLAG_INIT_DONE = true;
			

		}	
			
	
	}
	
//	if (!nrf_gpio_pin_read(CHARGING_PIN))	 //if charging ongoing
//	{
//	
//		if (previous_sleep_mode_timeout_counter >= sleep_mode_timeout_counter + 10)  //if 10 seconds have passed
//		{	
//				nrf_gpio_pin_write(BATT_SAM_EN,1);  //+VICTORISE - turn it on when in used
//				nrf_drv_saadc_sample();  // here we sample the batt voltage, clls saadc_callbacka	 
//				previous_sleep_mode_timeout_counter = sleep_mode_timeout_counter;
//			

//				if (!G_FLAG_BATTERY_OK)
//				{		  
//					prepare_sleep_mode();
//					G_FLAG_INIT_DONE = false;
//					sd_power_system_off(); //go in Low power mode
//					//nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);					
//				}
//			
//		}		
//	}
				
}

/**@brief Function for timer handler RGB (normal mode)
 */
static void timer_timeout_handler_rgb_display(void * p_context)
{
      
		uint8_t reg[2];

		G_NORMAL_MODE_TOTAL_TIME_COUNTER--;
		G_NORMAL_MODE_LIGHT_TIME_COUNTER--;   
				
		if (G_NORMAL_MODE_TOTAL_TIME_COUNTER ==0) // total time is finished
		{	
				
			//- shut down all the colors
			turn_off_RGB_color();	

			app_timer_stop(rgb_display_timer); //stop the timer
		
			nrf_gpio_pin_write(LED_EN,0); //disable the driver for power saving

		}
		else 
		{	
			if (G_NORMAL_MODE_LIGHT_TIME_COUNTER == 0)
			{	

				G_NORMAL_MODE_LIGHT_TIME_COUNTER = G_NORMAL_MODE_LIGHT_TIME_COUNTER_VAL;

				if (G_LIGHT_LED_ON)
				{
					
					G_LIGHT_LED_ON = false;

					//- shut down all the colors
					turn_off_RGB_color();
					
				}
				else
				{	
					
					G_LIGHT_LED_ON = true;
					
					RGB_driver_display_mode(G_COLOR); //then light on again the LED
					
				}	
			}	
		}		
				
}

/**@brief Function for timer handler RGB (DEMO mode)
 */
static void timer_timeout_handler_rgb_display_demo_mode(void * p_context)
{

		uint8_t reg[2];
	
		G_DEMO_MODE_TOTAL_TIME_COUNTER--; //this counter managed the whole DEMO mode 
		G_DEMO_MODE_BLINK_COUNTER--; //this counter is used for blink mode and also turned off mode (no need to have 2 differents counters: one can do both)

		if (G_DEMO_MODE_TOTAL_TIME_COUNTER == 0)  //of the DEMO mode is now finished
		{	
			
			// DEMO mode is over 			

			//- shut down all the colors
			turn_off_RGB_color();		
			
			nrf_gpio_pin_write(LED_EN,0);
			app_timer_stop(rgb_display_timer);  
			app_timer_stop(rgb_display_demo_mode_timer);  	
		}

		else if (G_BLINK_MODE && G_DEMO_MODE_BLINK_COUNTER == 0) //if we are in blink mode but it's now over then we go to turned off mode
		{
			G_BLINK_MODE=false;
			G_TURNED_OFF_MODE=true;
			G_DEMO_MODE_BLINK_COUNTER = 2*G_DEMO_MODE_BLINK_DURATION_COUNTER_VAL;	//the turned off mode lasts twice the blink mode
			
			//- shut down the current COLOR
			if (G_COLOR==1)		reg[0] = 0x02; //R channel
			if (G_COLOR==2)		reg[0] = 0x03; //G channel	
			if (G_COLOR==3)		reg[0] = 0x04; //B channel	
			reg[1] = 0x00;
			nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
			nrf_delay_ms(I2C_DELAY);	
			while (m_xfer_done == false);
			
			app_timer_stop(rgb_display_timer); //stop the timer
			
		}

		else if (G_TURNED_OFF_MODE && G_DEMO_MODE_BLINK_COUNTER == 0) //if we are in turned off mode but it's now over then we start over the blink mode
		{
			
			(G_COLOR==3)? (G_COLOR=1):(G_COLOR++);  //go to next color (in sequence: red then green then blue etc)
			RGB_driver_demo_mode();
		
		}	
				
}

/**@brief Function to start RGB display within demo mode
 */
static void RGB_driver_demo_mode(void)
{
	
		/************************ begin normal mode */
		G_NORMAL_MODE_TOTAL_TIME_COUNTER = G_DEMO_MODE_BLINK_DURATION_COUNTER_VAL; //total time the LED is blinking in demo mode (e.g. 2.5 seconds)			
		G_NORMAL_MODE_LIGHT_TIME_COUNTER = G_NORMAL_MODE_LIGHT_TIME_COUNTER_VAL = 200 / FIXED_TIMEOUT_VALUE;  // in demo mode, preiod of blikning is set to 200ms				

		RGB_driver_display_mode(G_COLOR);

		app_timer_start(rgb_display_timer,APP_TIMER_TICKS(FIXED_TIMEOUT_VALUE), NULL);	//timer_timeout_handler_rgb_display	
		/************************ end normal mode */		

		/************************ begin demo mode */				
		G_DEMO_MODE_BLINK_COUNTER =  G_DEMO_MODE_BLINK_DURATION_COUNTER_VAL; //total time the LED is blinking in demo mode (e.g. 2.5 seconds)
		
		G_BLINK_MODE = true;
		G_TURNED_OFF_MODE=false;				
		/************************ end demo mode */		

}	


/**@brief Function to turn off all the RGB colors 
 */
static void turn_off_RGB_color(void)
{
	
		uint8_t reg[2];
	
		//- shut down all the colors
		reg[0] = 0x02; //R channel
		reg[1] = 0x00;
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);					
		reg[0] = 0x03; //G channel
		reg[1] = 0x00;
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);	
		reg[0] = 0x04; //B channel
		reg[1] = 0x00;
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);		

}	

static void enable_RGB_color(void)
{

		// when the colors have to be lighted up
		uint8_t reg[2];

		reg[0] = 0x00;
		reg[1] = 0x40;
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		//enable the channels
		reg[0] = 0x01;
		reg[1] = 0x3F;
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);	

}	

/**@brief Set and get config value
 *
 */
static void manage_device_config(char * message,uint16_t message_length)
{

	uint8_t reg[2];
	uint32_t err_code=0;
	uint8_t value=0;
	uint16_t length=7;
	uint8_t i2c_adress;
	char return_string[9]="";
	char error_type[1];
	bool keep_going=true; //this is for asynchronus part (like battery level is called in a handler)

	/* message format = 8abccdd where:
			-8 is a constant
			-a is device_id  ('0' generic; '1'= accelero; '2'= rgb_driver)
			-b is "R"ead or "W"rite
			-cc is regiter id
			-dd is register value
	*/
	// example: 81R0F   (38 31 52 30 46)   				--> read the WHO_I_AM register of accelero, must result 0x3300 (0x52 is letter 'R')
	// example: 81R32   (38 31 52 33 32) 					--> read INT1_THS	register of accelero (0x32)
	// example: 81W32FF (38 31 57 33 32 46 46)   	--> write 'FF' in INT1_THS	register of accelero (0x32)
	// example: 81R36   (38 31 52 33 36) 					--> read INT2_THS	register of accelero (0x36)	
	// example: 81W36FF (38 31 57 33 36 46 46)   	--> write 'FF' in INT2_THS	register of accelero (0x36)	
	// example: 80R10   (38 30 52 31 30) 					--> get Battery level  (code is '10')

		switch (message[1]) 
		{
			
			case '0': //generic is to change attributes not related to a device such as Battery level
				break;			
			
			case '1': //accelerometer	
						i2c_adress = ACCEL_I2C_ADDR;
				break;
			
			case '2': //RGB driver
						i2c_adress = RGB_DRIV_I2C_ADDR;
			
				break;			

			default: {*error_type = 'F'; err_code=1;}				
		}	

		if (err_code==0)
		{
			reg[0] = convert_to_hex(&message[3],&message[4]);  //concantenate these char to get a hex register_id (required for Read or Write)
		}

		if (err_code==0)
		{		
				switch (message[2]) //Read or Write?
				{	

					case 'W': 		//WRITE (0x57)
						
								if (message_length <7) {*error_type = 'F';err_code=3; break;}  //it means the register_id and/or register_value not been sent

								reg[1] = convert_to_hex(&message[5],&message[6]);  //register_value : concantenate these char to get a hex value	
								
								/* Wake up Threshold */
								if (reg[0] == INT1_THS) G_ACCELERO_THRESHOLD_INT1 = reg[1];
								
								/* Crash test Threshold */
								static flash_storage_t flash_storage = {0};
								if (reg[0] == INT2_THS)
								{			
									flash_storage = save_data_in_flash('W',1,reg[1]);
									if (flash_storage.err_code != 0) {*error_type = 'M';err_code=flash_storage.err_code; break;} 
								}
									
								/* RGB brightness */
								if (reg[0] == 0x05 || reg[0] == 0x06 || reg[0] == 0x07)
								{	
									(void) save_data_in_flash('W',2,reg[1]);
									manage_rgb_current(reg[1]);
									if (flash_storage.err_code != 0) {*error_type = 'M';err_code=flash_storage.err_code; break;} 								
								}
								
								err_code=nrf_drv_twi_tx(&m_twi, i2c_adress, reg, sizeof(reg), false);
								nrf_delay_ms(I2C_DELAY);	
								while (m_xfer_done == false);	
								
								message[2] = 'R';  // we force a read after a Write to verify that the update has been ok
								message_length = 5; //with a read, the message to be taken into account must be 5 char 
					
					case 'R': 		//READ (0x52)
					
								if (message_length != 5) {*error_type = 'F';err_code=4; break;}  //it means the register_id not sent or too much char sent
								
								switch (message[1]) 
								{							
										case '0': // for a generic value (not related to a device)
					
											switch (message[3])
											{		

												case '0': //Firmare version
													
													value=FW_VERSION_MINOR;
  												break;	
												
												case '1': //Battery level 
						
													keep_going = false;  //because the voltage sampling function (saadc_callback) is asynchronos (the voltage value is sent to ble in saadc_callback
												
													//nrf_gpio_pin_write(BATT_SAM_EN,1);    //enable sampling part
													nrf_delay_ms(200); //wait a delay before sampling
													nrf_drv_saadc_sample();  // here we sample the batt voltage, calls saadc_callback	
													break;				
									
											}
											break;

										case '1': //accelerometer
										case '2': //RGB Driver
										{	

											err_code = nrf_drv_twi_tx(&m_twi, i2c_adress, reg, 1, false);
											nrf_delay_ms(I2C_DELAY);	
											while (m_xfer_done == false);
											if (err_code == 0)
											{
												value = 0;
												err_code = nrf_drv_twi_rx(&m_twi, i2c_adress, &value, 1);
												nrf_delay_ms(I2C_DELAY);	
												while (m_xfer_done == false);
											} else {*error_type = 'T';}
										}	
										break;								
									}
					break;	// end case R
						
					default: {*error_type = 'F';err_code=2;}				
						
				} // end switch W or R
		}
		
		if (keep_going) //asynchronus parts end elsewhere so we must prevent to send data twice (like battery level is managed in an handler)
		{	
				
			strncat(return_string,message,message_length); //first, the output contains the input packet
			
			if (err_code!=0) //an error has been generated ahead
			{
				char ch[4];
				sprintf(ch, "%d", err_code);
				strcat(return_string,"-ERR-");
				strcat(return_string,error_type);
				strcat(return_string,ch);
				length = 15;
			}	
			else 
			{ 
				uint8_t half_byte;					
				half_byte= (uint8_t)(value >> 4);
				half_byte = convert_to_ascii(&half_byte);
				strcat(return_string,&half_byte);
				
				half_byte	= (uint8_t)(value << 4)/16;
				half_byte = convert_to_ascii(&half_byte);
				strcat(return_string,&half_byte);
			}
			ble_nus_data_send(&m_nus,return_string, &length, m_conn_handle); 			
			free(return_string);
		}
}
/**@brief convert a 2 digits to Hex format
 *
 */
static uint8_t convert_to_hex(char * MSB, char * LSB)
{
		uint8_t hex_number;

		if(*MSB >='0' && *MSB<='9') {hex_number = 16*(*MSB - 48);}
		if(*MSB >='A' && *MSB<='F') {hex_number = 16*(*MSB - 65 + 10);}	

		if(*LSB >='0' && *LSB<='9') {hex_number += *LSB - 48;}
		if(*LSB >='A' && *LSB<='F') {hex_number += *LSB - 65 + 10;}		
		
		return hex_number;
}	
/**@brief convert a 2 digits to ASCII
 *
 */
static uint8_t convert_to_ascii(uint8_t *number)
{
		if(*number >=10 && *number<=15) *number +=55;
		if(*number >=0 && *number<=9) *number +=48;	
		
		return *number;
}	

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_INFO("Connected to a previously bonded device.");
        } break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            NRF_LOG_INFO("Connection secured: role: %d, conn_handle: 0x%x, procedure: %d.",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            advertising_start(false);
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
            // This can happen when the local DB has changed.
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{

    // Initialize timer module.
    uint32_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Create timers.
//app_timer_create(&wakeup_mode_timer,APP_TIMER_MODE_REPEATED,timer_timeout_handler_wakeup_mode);

//		// timer for RGB LED displaying		
//		app_timer_create(&rgb_display_timer,APP_TIMER_MODE_REPEATED,timer_timeout_handler_rgb_display); 
//		app_timer_create(&rgb_display_demo_mode_timer,APP_TIMER_MODE_REPEATED,timer_timeout_handler_rgb_display_demo_mode);	
}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    /* YOUR_JOB: Use an appearance value matching the application's use case.
       err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_);
       APP_ERROR_CHECK(err_code); */

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

 
/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t                  err_code;
    nrf_ble_qwr_init_t        qwr_init  = {0};
    ble_dfu_buttonless_init_t dfus_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize the async SVCI interface to bootloader.
    err_code = ble_dfu_buttonless_async_svci_init();
    APP_ERROR_CHECK(err_code);

    dfus_init.evt_handler = ble_dfu_evt_handler;

    err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

		// Initialize NUS.
		ble_nus_init_t     nus_init;
		memset(&nus_init, 0, sizeof(nus_init));

		nus_init.data_handler = nus_data_handler;

		err_code = ble_nus_init(&m_nus, &nus_init);

}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{

	// Prepare wakeup buttons.
	//err_code = bsp_btn_ble_sleep_mode_prepare();
	//APP_ERROR_CHECK(err_code);
	//Disable SoftDevice. It is required to be able to write to GPREGRET2 register (SoftDevice API blocks it).
	//GPREGRET2 register holds the information about skipping CRC check on next boot.
	nrf_sdh_disable_request();

	// Go to system-off mode (this function will not return; wakeup will cause a reset).
	sd_power_system_off();   
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
	uint32_t err_code;

	switch (ble_adv_evt)
	{
			case BLE_ADV_EVT_FAST:
					err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
					APP_ERROR_CHECK(err_code);
					break;

			case BLE_ADV_EVT_IDLE:
					sleep_mode_enter();
					break;

			default:
					break;
	}
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            // BEGIN VICTORISE
						if (nrf_gpio_pin_read(CHARGING_PIN)) //if we are NOT in charging mode then prepare sleep mode
						{		
							prepare_sleep_mode();
				    }
						// END VICTORISE
            break;

        case BLE_GAP_EVT_CONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
						nrf_drv_twi_enable(&m_twi); //VICTORISE: very important else twi fails when reconnecting 
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function before going in Sleep mode */
static void prepare_sleep_mode (void)
{
		
		//app_timer_stop(wakeup_mode_timer); //stop timer     //**** HW fixing   
		app_timer_stop(rgb_display_timer); //stop timer if it is running (or not)
		app_timer_stop(rgb_display_demo_mode_timer); //stop timer if it is running (or not)	
	
		G_START_WAKEUP_MODE_TIMER=true; //when the device will wakeup afterwards
	
//**** HW fixing  	
//		// disable all the RGB channels
//		uint8_t reg[2];
//		reg[0] = 0x01;
//		reg[1] = 0x00;
//		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
//		nrf_delay_ms(I2C_DELAY);	
//		while (m_xfer_done == false);					
//**** HW fixing  
	
		nrf_gpio_pin_write(LED_EN,0); //disable rgb driver to save power	

	  nrf_drv_twi_disable(&m_twi); //disable TWI for Low power mode
		nrf_delay_ms(I2C_DELAY);	
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init()
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/** @brief Clear bonding information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t               err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = false;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

/**@brief   Function for initializing the GATT module.
 * @details The GATT module handles ATT_MTU and Data Length update procedures automatically.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
}

static void power_management_init(void)
{
    uint32_t err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}
/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{
	static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
	static uint8_t index = 0;
	uint32_t       err_code;

	switch (p_event->evt_type)
	{
			case APP_UART_DATA_READY:
					UNUSED_VARIABLE(app_uart_get(&data_array[index]));
					index++;

					if ((data_array[index - 1] == '\n') || (index >= (m_ble_nus_max_data_len)))
					{
							NRF_LOG_DEBUG("Ready to send data over BLE NUS");
							NRF_LOG_HEXDUMP_DEBUG(data_array, index);

							do
							{
									uint16_t length = (uint16_t)index;
									err_code = ble_nus_data_send(&m_nus, data_array, &length, m_conn_handle);
									if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) &&
											 (err_code != NRF_ERROR_NOT_FOUND) )
									{
											APP_ERROR_CHECK(err_code);
									}
							} while (err_code == NRF_ERROR_BUSY);

							index = 0;
					}
					break;

			case APP_UART_COMMUNICATION_ERROR:
					APP_ERROR_HANDLER(p_event->data.error_communication);
					break;

			case APP_UART_FIFO_ERROR:
					APP_ERROR_HANDLER(p_event->data.error_code);
					break;

			default:
					break;
	}
}
/**@snippet [Handling the data received over UART] */

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_init(void)
{
    uint32_t                     err_code;
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
#if defined (UART_PRESENT)
        .baud_rate    = NRF_UART_BAUDRATE_115200
#else
        .baud_rate    = NRF_UARTE_BAUDRATE_115200
#endif
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
	
}

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
							//data_handler(m_sample);
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * @brief TWI initialization. =>  I2C
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_LP55231_config = {
       .scl                = ARDUINO_SCL_PIN,
       .sda                = ARDUINO_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_LP55231_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}
/**
 * @brief Function triggered by accelerometer (pin INT1) to wake-up the device
 */
static void in_pin_handler_INT1(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{

	sleep_mode_timeout_counter = SLEEP_MODE_TIMEOUT_COUNTER_VALUE; //whenever we receive an INT1 from accelero then we delay again the time to go to sleep
	
	if (G_START_WAKEUP_MODE_TIMER)  //this prevents to start again the timer whereas it's already running
	{	
		app_timer_start(wakeup_mode_timer,APP_TIMER_TICKS(FIXED_TIMEOUT_VALUE_1S), NULL);  //timer_timeout_handler_wakeup_mode
		nrf_drv_twi_enable(&m_twi); //enable TWI
		manage_rgb_current(0xFF);
		//manage_uart_message("130000100",9);  //flashing once when waking up,  blue color
		G_START_WAKEUP_MODE_TIMER=false;
	}

	//read INT1_SRC to release latch
	uint8_t reg[2];
	reg[0] = INT1_SRC; 
	uint8_t value=0;
	nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, 1, false);
	nrf_delay_ms(I2C_DELAY);	
	while (m_xfer_done == false);
	nrf_drv_twi_rx(&m_twi, ACCEL_I2C_ADDR, &value, 1);
	nrf_delay_ms(I2C_DELAY);
	while (m_xfer_done == false);	
	
}
/**
 * @brief Function triggered by accelerometer (pin INT2) for CRASH DETECTION
 */
static void in_pin_handler_INT2(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	
	uint8_t reg[2];

	//read INT2_SRC to release latch
	reg[0] = INT2_SRC; 
	uint8_t value=0;
	nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, 1, false);
	nrf_delay_ms(I2C_DELAY);	
	while (m_xfer_done == false);
	nrf_drv_twi_rx(&m_twi, ACCEL_I2C_ADDR, &value, 1);
	nrf_delay_ms(I2C_DELAY);
	while (m_xfer_done == false);	
	
	uint8_t return_string[9]="80R90";
	uint16_t length=9;
	ble_nus_data_send(&m_nus,return_string, &length, m_conn_handle);
		
}

/**
 * @brief Function triggered by charging module (pin CHARGING_PIN) 
 */
static void in_pin_handler_CHARGING_PIN(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	//manage_uart_message("110000100",9);  //just for the first time the interrupt is recevied (more likely to be uncharged)
	
	if (G_START_WAKEUP_MODE_TIMER)  //this prevents to start again the timer whereas it's already running
	{	
		sleep_mode_timeout_counter = SLEEP_MODE_TIMEOUT_COUNTER_VALUE; //if the charging wakes-up the device, then we set the counter once for all
//		app_timer_start(wakeup_mode_timer,APP_TIMER_TICKS(FIXED_TIMEOUT_VALUE_1S), NULL);  //timer_timeout_handler_wakeup_mode
//		nrf_drv_twi_enable(&m_twi); //enable TWI
		G_START_WAKEUP_MODE_TIMER=false;
	}	

	previous_sleep_mode_timeout_counter = sleep_mode_timeout_counter;   //every 3 seconds
	//manage_rgb_current(0x20);	//lower the current in charging mode not to drain the battery
}

/**
 * @brief Function for configuring: PIN_IN pin for input, PIN_OUT pin for output,
 * and configures GPIOTE to give an interrupt on pin change.
 */
static void gpio_init(void)
{
    ret_code_t err_code;

    if(!nrf_drv_gpiote_is_init())
        nrf_drv_gpiote_init();
	
		// GPIOTE config for INT1
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    in_config.pull = GPIO_PIN_CNF_PULL_Disabled;  //This input pin managed by accelerometer

    err_code = nrf_drv_gpiote_in_init(ACC_INT1, &in_config, in_pin_handler_INT1);
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(ACC_INT1, true);
		
		//this is very important for Sleep mode (the GPOITE doesn't wake up itself from Sleep mode)
		nrf_gpio_cfg_sense_input(ACC_INT1, GPIO_PIN_CNF_PULL_Disabled, NRF_GPIO_PIN_SENSE_HIGH); 
		nrf_gpio_cfg_sense_input(ACC_INT2, GPIO_PIN_CNF_PULL_Disabled, NRF_GPIO_PIN_SENSE_HIGH); 					
		
		// GPIOTE config for INT2 (crash test)
		err_code = nrf_drv_gpiote_in_init(ACC_INT2, &in_config, in_pin_handler_INT2);
		APP_ERROR_CHECK(err_code);
  	nrf_drv_gpiote_in_event_enable(ACC_INT2, true);	

    // GPIOTE config for Charging PIN
//		nrf_drv_gpiote_in_config_t in_config_CHARGING_PIN = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
//		in_config_CHARGING_PIN.pull = GPIO_PIN_CNF_PULL_Pullup; //this pin is open drain so must be pulled up
//		err_code = nrf_drv_gpiote_in_init(CHARGING_PIN, &in_config_CHARGING_PIN, in_pin_handler_CHARGING_PIN);
//		APP_ERROR_CHECK(err_code);

		//this is very important for Sleep mode (the GPOITE doesn't wake up itself from Sleep mode)
//		nrf_gpio_cfg_sense_input(CHARGING_PIN, GPIO_PIN_CNF_PULL_Pullup, NRF_GPIO_PIN_SENSE_LOW);	//it's set to low when charging

//		nrf_drv_gpiote_in_event_enable(CHARGING_PIN, true);		
}

/**
 * @brief Function handler for flash storage
 */
static void fds_evt_handler(fds_evt_t const * p_evt)
{

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
            }
            break;	
						
        default:
            break;
    }	
	
}	

/**@brief   Wait for fds to initialize. */
void wait_for_flash_ready(nrf_fstorage_t const * p_fstorage)
{
    /* While fstorage is busy, sleep and wait for an event. */
    while (nrf_fstorage_is_busy(p_fstorage))
    {
      (void) sd_app_evt_wait();  
			//idle_state_handle();
    }
}
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
}
NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = FSTORAGE_START_ADDR,
    .end_addr   = FSTORAGE_END_ADDR,
};
/**@brief   Helper function to obtain the last address on the last page of the on-chip flash that
 *          can be used to write user data.
 */
static uint32_t nrf5_flash_end_addr_get()
{
    uint32_t const bootloader_addr = NRF_UICR->NRFFW[0];
    uint32_t const page_sz         = NRF_FICR->CODEPAGESIZE;
    uint32_t const code_sz         = NRF_FICR->CODESIZE;

    return (bootloader_addr != 0xFFFFFFFF ?
            bootloader_addr : (code_sz * page_sz));
}
/**
 * @brief Function to save in Flash memory some variables 
 */
static flash_storage_t save_data_in_flash(char input_action, uint8_t input_key ,uint32_t input_value)
{

	/*
	input_action: 'R' for Read; 'W' for write
	input_key: 
						- 1 for crash test threshold
						- 2 for RGB brightness (common value for the 3 colors)
	input_value = value to be written to flash memory
	*/
		uint8_t err_code;	

		static uint32_t	flash_data;
		static uint32_t stored_data;
		static uint32_t fstorage_start_addr;
		flash_data = 0x45594500; //we put this word "EYE" to be sure that the value in the flash memory is not random
		flash_data |= input_value; //then the value will be right-justified
	
		//flash_data = (uint32_t)input_value;	
    nrf_fstorage_api_t * p_fs_api;
		flash_storage_t output_flash_storage;
		output_flash_storage.error_id = 0;
		output_flash_storage.err_code = NRF_SUCCESS;
		output_flash_storage.output_data = 0;
	
		//(void) nrf5_flash_end_addr_get();
		p_fs_api = &nrf_fstorage_sd; //because Soft device is present here
		
		switch(input_key)
		{
			case 1:
				fstorage_start_addr = FSTORAGE_START_ADDR;
				break;
			case 2:
				fstorage_start_addr = FSTORAGE_START_ADDR + 4096;
				break;			
			default:
				break;				
				
		}			
		
		err_code = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
		
		if (err_code == NRF_SUCCESS)
		{		
				switch (input_action)
				{	
					case 'R':
					{
						err_code = nrf_fstorage_read(&fstorage, fstorage_start_addr,&stored_data ,sizeof(stored_data));
						
						if (stored_data >> 8 == 0x00455945) //it begins by "EYE" so not a random value
						{
							output_flash_storage.output_data = (uint8_t)stored_data;
						}	
						else
						{
							output_flash_storage.output_data = 0;
						}							
							
						
						if (err_code != NRF_SUCCESS ) 
						{
							output_flash_storage.error_id = 2;	
						}	
						
						break;	
					}
					case 'W':
					{	
							err_code = nrf_fstorage_erase(
																	&fstorage,   						/* The instance to use. */
																	fstorage_start_addr,    /* The address of the flash pages to erase. */
																	1, 											/* The number of pages to erase. */
																	NULL);       						/* Optional parameter, backend-dependent. */
					
						if (err_code == NRF_SUCCESS )
						{
							err_code = nrf_fstorage_write(&fstorage, fstorage_start_addr,&flash_data, sizeof(flash_data), NULL);
							
							if (err_code != NRF_SUCCESS ) 
							{	
								output_flash_storage.error_id = 4;
							}
						}
						else 
						{
							output_flash_storage.error_id = 3;	
						}
						
						break;
					}
				//wait_for_flash_ready(&fstorage);
				}
		} 
		else 
		{
			output_flash_storage.error_id = 1;		
		}

		output_flash_storage.err_code = err_code;
		return output_flash_storage;
		
}
	
/**
 * @brief Function to initialize the accelerometer
 */
static void accelero_init()
{
	
		uint8_t reg[2];
		uint8_t err_code;	
	  
	  sleep_mode_timeout_counter = SLEEP_MODE_TIMEOUT_COUNTER_VALUE;  //it must be initialized here also (because we start in Sleep so INT1 is not interrupting first)
	  
	// Enable I2C for accelero
		nrf_gpio_cfg_output(ACC_CS);
		nrf_gpio_pin_set(ACC_CS);
		nrf_delay_ms(500); 

		/***  BEGIN INIT   ***********************************************************/	
    reg[0] = CTRL_REG1;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG2;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG3;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG4;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG5; 
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
		
		reg[0] = CTRL_REG6;  
		reg[1] = 0x00;		
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);			
		
		reg[0] = INT1_THS;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = INT1_DURATION;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
		
		reg[0] = INT1_CFG;
		reg[1] = 0x00;			
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);			

		reg[0] = INT2_THS;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = INT2_DURATION;
		reg[1] = 0x00;	
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
		
		reg[0] = INT2_CFG;
		reg[1] = 0x00;			
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);		
		/***  END INIT   *************************************************************/
	
		reg[0] = CTRL_REG1;
		reg[1] = 0x2F;		//0x2F Low power mode (10 Hz); all axis //0x5F Low power mode (100 Hz) 
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG2;
		reg[1] = 0x03;		// High-pass filter for int1 and int2
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG3;
		reg[1] = 0x40 ;		 // AOI1 interrupt on INT1 
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG4;
		//reg[1] = 0x80  ;		//  Full Scale = +/-2 g 
		//reg[1] = 0x10  ;		//  Full Scale = +/-4 g 
		reg[1] = 0x20  ;		//  Full Scale = +/-8 g
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = CTRL_REG5; 
		//reg[1] = 0x08 ;		// Latch interrupt on INT1  but NOT on INT2
		//reg[1] = 0x00 ;		// NOT Latch interrupt on INT1  and  NOT on INT2
		reg[1] = 0x0A ;		// Latch interrupt on INT1 AND Latch on INT2		
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
		
		reg[0] = CTRL_REG6; 
		reg[1] = 0x20 ;		// activate INT2
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);			

//******** INT1 Config for WAKE-UP INTERRUPT ********************
		//@FS=2g  -> 1LSb = 16mg   (e.g.-> 0x10*16mg = threshold 256mg) (e.g.-> 0x40*16mg = threshold 1024mg)  preferred default = 0x10 	
		//@FS=4g  -> 1LSb = 32mg   (e.g.-> 0x01*32mg = threshold 32mg) (e.g.-> max value = 0x7F*32mg = threshold 4g)  
		//@FS=8g  -> 1LSb = 63mg   (e.g.-> 0x01*63mg = threshold 63mg) (e.g.-> max value = 0x7F*63mg = threshold 8g)  		
		reg[0] = INT1_THS;
		reg[1] = G_ACCELERO_THRESHOLD_INT1;  
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = INT1_DURATION; //beware: on 6 digits only!
		reg[1] = 0x00 ;		 //   [ODR=100HZ -> 1LSB = 10ms]  [ODR=10HZ -> 1LSB = 100ms]
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = INT1_CFG;
		reg[1] = 0xAA  ;		// AND combination of interrupt events  / all interrupts HIGH on XYZ 
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);				

//		reg[0] = WHO_AM_I; 
//		uint8_t value=0;
//		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, 1, false);
//		nrf_delay_ms(I2C_DELAY);	
//		while (m_xfer_done == false);
//		err_code = nrf_drv_twi_rx(&m_twi, ACCEL_I2C_ADDR, &value, 1);
//		nrf_delay_ms(I2C_DELAY);
//		while (m_xfer_done == false);

//******** INT2 Config for CRASH DETECTION ********************
		//@FS=2g  -> 1LSb = 16mg   (e.g.-> 0x10*16mg = threshold 256mg) (e.g.-> 0x40*16mg = threshold 1024mg)  preferred default = 0x10 	
		//@FS=4g  -> 1LSb = 32mg   (e.g.-> 0x01*32mg = threshold 32mg) (e.g.-> max value = 0x7F*32mg = threshold 4g)  
		//@FS=8g  -> 1LSb = 63mg   (e.g.-> 0x01*63mg = threshold 63mg) (e.g.-> max value = 0x7F*63mg = threshold 8g) 
		//the crash test treshold is a customized value so to be retrieved fist from flash memory
		reg[0] = INT2_THS;
		static flash_storage_t crash_test_ths;
		crash_test_ths = save_data_in_flash('R',1,NULL);
		if (crash_test_ths.err_code == 0 & crash_test_ths.output_data != 0) 
		{
			reg[1] = crash_test_ths.output_data;
		}
  	else 
		{			
			reg[1] = G_ACCELERO_THRESHOLD_INT2;
		}	
		
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = INT2_DURATION; //beware: on 6 digits only!
		reg[1] = 0x00 ;		 //7F = 12.7 seconds 3F=6.3 seconds   [ODR=10HZ -> 1LSB = 1/10HZ = 100ms]  [ODR=100HZ -> 1LSB = 10ms]  DEFAULT = 0x7F
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);

		reg[0] = INT2_CFG;
		reg[1] = 0xAA  ;		// AND combination of interrupt events  / all interrupts LOW XYZ 
		err_code = nrf_drv_twi_tx(&m_twi, ACCEL_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);			
//************************************************************

} //end accelero_init()

/**@brief Function initialize the RGB driver
 */
static void RGB_driver_init(void)
{

	// Enable LED driver
	nrf_gpio_cfg_output(LED_EN);
	nrf_gpio_pin_write(LED_EN,1); //enable driver
	nrf_delay_ms(1); //1 ms delay 

	ret_code_t err_code;
	uint8_t reg[2];

	enable_RGB_color();

	reg[0] = 0x08;
	reg[1] = 0x11;   //charge pump 1.5x mode and use internal clock:
	err_code = nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
	nrf_delay_ms(I2C_DELAY);	
	while (m_xfer_done == false);	
	
	//set the current for each color
	static flash_storage_t value_from_flash;
	value_from_flash = save_data_in_flash('R',2,NULL);
	if (value_from_flash.err_code == 0 & value_from_flash.output_data != 0) 
	{
		reg[1] = value_from_flash.output_data;
	}
	else 
	{			
		reg[1] = 0xff;
	}		
	manage_rgb_current(reg[1]);

	//shut down colors (PWM = 0)
	turn_off_RGB_color();		

	nrf_gpio_pin_write(LED_EN,0); //disable RGB driver to save power

}

void manage_rgb_current(uint8_t current)
{
	uint8_t reg[2];

	reg[0] = 0x05;
	reg[1] = current; //RED current
	nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
	nrf_delay_ms(I2C_DELAY);	
	while (m_xfer_done == false);

	reg[0] = 0x06;
	reg[1] = current; //GREEN current
	nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
	nrf_delay_ms(I2C_DELAY);	
	while (m_xfer_done == false);

	reg[0] = 0x07;
	reg[1] = current; //Blue current
	nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
	nrf_delay_ms(I2C_DELAY);	
	while (m_xfer_done == false);		

}	
/**@brief Function to light up the RGB LED
 *
 * @details Lights up 1 color of the RGB 
 *
 * @param[in]   COLOR   			The Color to light up
 */
static void RGB_driver_display_mode(uint8_t COLOR)
{

	uint8_t reg[2], pwm_r, pwm_g, pwm_b;

	if 			( COLOR == 0) { pwm_r = 0	; pwm_g =0 			; pwm_b = 0 	; }	
	else if ( COLOR == 1) { pwm_r = 255	; pwm_g =0 		; pwm_b = 0 	; }
	else if (	COLOR	== 2)	{ pwm_r = 0	 	; pwm_g =255 	; pwm_b = 0 	; }
	else if (	COLOR	== 3)	{ pwm_r = 0	 	; pwm_g =0 		; pwm_b = 255 ; }
	else if (	COLOR	== 4)	{ pwm_r = 100 ; pwm_g =50 	; pwm_b = 0 	; }
	else if (	COLOR	== 5)	{ pwm_r = 220 ; pwm_g =20		; pwm_b = 0		; }
	else if (	COLOR	== 6)	{ pwm_r = 220	; pwm_g =0 		; pwm_b = 50 	; }
	else if (	COLOR	== 7)	{ pwm_r = 150	; pwm_g =0 		; pwm_b = 200 ; }
	else if (	COLOR	== 8)	{ pwm_r = 220	; pwm_g =180 	; pwm_b = 180 ; }		
	else if (	COLOR	== 9)	{ pwm_r = 50	; pwm_g =30 	; pwm_b = 30 	; }		
		
	if (pwm_r) 
	{
		reg[0] = 0x02; //R channel	
		reg[1] = pwm_r; //PWM
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
	}
	if (pwm_g) 
	{
		reg[0] = 0x03; //G channel	
		reg[1] = pwm_g; //PWM
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
	}
	if (pwm_b) 
	{
		reg[0] = 0x04; //B channel	
		reg[1] = pwm_b; //PWM
		nrf_drv_twi_tx(&m_twi, RGB_DRIV_I2C_ADDR, reg, sizeof(reg), false);
		nrf_delay_ms(I2C_DELAY);	
		while (m_xfer_done == false);
	}
		
}

/**@brief Function called whenever is sampling of Battery voltage is requested (i.e. when nrf_drv_saadc_sample() is called)
 */
void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{

	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
	{

		nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);

		uint16_t level = p_event->data.done.p_buffer[0]; // this is the sampled voltage; as stated in sdk_config.h, it's a 10 bits resolution, Gain 1/6 and ref = 0.6V

		//formula: Voltage = VALUE_FOUND * 0.6 / 1024 * 6 * 2 (+0.6V tocompensate the voltage drop of the transistor in the sampling circuit)
		batteryVoltage = (level * 0.6 / 1024 *6 *2) + 0.6;   //when external voltage (from charging circuit)
		//batteryVoltage = (level * 0.6 / 1024 *6);   //when Vdd 
		
		if (batteryVoltage < 2.7 )
		{	
			G_FLAG_BATTERY_OK = false;
			G_FLAG_INIT_DONE = false;
		}	
		else
		{
			G_FLAG_BATTERY_OK = true;

			if (G_FLAG_INIT_DONE)
			{	
//					if (!nrf_gpio_pin_read(CHARGING_PIN)) //if charging ongoing
//					{
//						//if (batteryVoltage>=2.1 && batteryVoltage<3.5) manage_uart_message("110000100",9);  //31 31 30 30 30 30 31 30 30 // red; just turned on; for 100ms
//						if (batteryVoltage>=3.5 && batteryVoltage<4.0) manage_uart_message("150000100",9);  //31 31 30 30 30 30 31 30 30 // yellow; just turned on; for 100ms			
//						if (batteryVoltage>=4.0) manage_uart_message("120000100",9);  											//31 31 30 30 30 30 31 30 30 // green; just turned on; for 100ms	
//					}			
				
					uint8_t return_string[9]="";

					uint16_t length=9;
					uint8_t half_byte;
					
					strcat(return_string,"80R10");  //actually it is coming from message[9] but for optimization, it's better to write it explicitly
					
					level = batteryVoltage * 100;
					
					half_byte= (uint8_t)(level / 1000);   
					half_byte = convert_to_ascii(&half_byte);
					strcat(return_string,&half_byte);
					
					half_byte	= (uint8_t)((level / 100) % 10);  
					half_byte = convert_to_ascii(&half_byte);
					strcat(return_string,&half_byte);

					half_byte= (uint8_t)((level / 10) % 10); 
					half_byte = convert_to_ascii(&half_byte);
					strcat(return_string,&half_byte);
					
					half_byte	= (uint8_t)(level % 10); 
					half_byte = convert_to_ascii(&half_byte);
					strcat(return_string,&half_byte);

					ble_nus_data_send(&m_nus,return_string, &length, m_conn_handle); //put battery level in RX of mobile phone				
			}

		}		

	}
	
	nrf_gpio_pin_write(BATT_SAM_EN,0);  //+VICTORISE - turn it off to save power  
}
/**@brief Function to initialize battery sampling 
 */
void saadc_init(void)
{
		nrf_saadc_channel_config_t channel_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN4);  // this is P0.28 "Batt_ADC"
		//nrf_saadc_channel_config_t channel_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_VDD);
		nrf_drv_saadc_init(NULL, saadc_callback);  //-> saadc_callback is the handler for saadc
    nrf_drv_saadc_channel_init(0, &channel_config);
    nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
}	

void init_all_services(void)
{

	bool erase_bonds;

	uart_init();  //+VICTORISE ->to be optimized: UART to be disabled   in sleeep mode		

  //twi_init(); //+VICTORISE - Initialization of the I2C
	//gpio_init(); //+VICTORISE for accelero  ->to be optimized: put low accuracy for this pin gpiote

//	nrf_gpio_cfg_output(BATT_SAM_EN);	 //+VICTORISE - PIN Enable for measuring battery level  
//	nrf_gpio_pin_write(BATT_SAM_EN,0);  //+VICTORISE - turn it off when not used  

    power_management_init();
    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();
	
	// Start execution.
	advertising_start();
		
//	//if we are not in charging mode then device woke up either by movement or by pressing reset /  we welcome user with a lighting up 		
//	if (nrf_gpio_pin_read(CHARGING_PIN)); //manage_uart_message("135006000",9); //open drain, pulled-up to high is when it's not charging
//	else in_pin_handler_CHARGING_PIN(NULL,NULL); //we force a call to the interrupt handler because the charging is already ongoing

//	RGB_driver_init();	 //+VICTORISE - RGB driver
//	accelero_init();	//+VICTORISE - accelerometer

	
}


/**@brief Function for application main entry.
 */
int main(void)
{

	/*
		bool erase_bonds;

    // Initialize.
    //uart_init();  //+VICTORISE ->to be optimized: UART to be disabled   in sleeep mode	
    timers_init(); 
    ret_code_t err_code = nrf_drv_clock_init(); //+VICTORISE invoking lfclk is mandatory else timer wouldn t start
		nrf_drv_clock_lfclk_request(NULL);  //+VICTORISE invoking lfclk is mandatory else timer wouldn t start
	  sleep_mode_timeout_counter = SLEEP_MODE_TIMEOUT_COUNTER_VALUE; // we want to delay by 30seconds the BLE stack to start 
		app_timer_start(wakeup_mode_timer,APP_TIMER_TICKS(FIXED_TIMEOUT_VALUE_1S), NULL);  //timer_timeout_handler_wakeup_mode	
	
//		gpio_init(); //+VICTORISE for accelero  ->to be optimized: put low accuracy for this pin gpiote
	
//		nrf_gpio_cfg_output(BATT_SAM_EN);	 //+VICTORISE - PIN Enable for measuring battery level  
//		nrf_gpio_pin_write(BATT_SAM_EN,0);  //+VICTORISE - turn it off when not used  

//	  power_management_init();
//    ble_stack_init();
//    peer_manager_init();
//    gap_params_init();
//    gatt_init();
//	  services_init();  //+VICTORISE Due to error in ble_advdata_encode! services_init() must be done before advertising_init()
//    advertising_init();
//    conn_params_init();

//		saadc_init(); //+VICTORISE - for batt voltage sampling
//		nrf_drv_saadc_sample();


		
//		//if we are not in charging mode then device woke up either by movement or by pressing reset /  we welcome user with a lighting up 		
//		if (nrf_gpio_pin_read(CHARGING_PIN)); //manage_uart_message("135006000",9); //open drain, pulled-up to high is when it's not charging
//		else in_pin_handler_CHARGING_PIN(NULL,NULL); //we force a call to the interrupt handler because the charging is already ongoing

*/

    uart_init();
    //log_init();
    timers_init();
    power_management_init();
    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();

    // Start execution.
//    printf("\r\nUART started.\r\n");
//    NRF_LOG_INFO("Debug logging for UART over RTT started.");
    advertising_start();

		
    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
	
	
}

